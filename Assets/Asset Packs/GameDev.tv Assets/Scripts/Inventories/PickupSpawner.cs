﻿using Newtonsoft.Json.Linq;
using UnityEngine;
using RPG.Saving.Json;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// Spawns pickups that should exist on first load in a level. This
    /// automatically spawns the correct prefab for a given inventory item.
    /// </summary>
    public class PickupSpawner : MonoBehaviour, IJsonSaveable
    {
        // CONFIG DATA
        [SerializeField] private InventoryItem item;
        [SerializeField] private int number = 1;

        // LIFECYCLE METHODS
        private void Awake()
        {
            // Spawn in Awake so can be destroyed by save system after.
            SpawnPickup();
        }

        // PUBLIC

        /// <summary>
        /// Returns the pickup spawned by this class if it exists.
        /// </summary>
        /// <returns>Returns null if the pickup has been collected.</returns>
        public Pickup GetPickup() 
        { 
            return GetComponentInChildren<Pickup>();
        }

        /// <summary>
        /// True if the pickup was collected.
        /// </summary>
        public bool IsCollected() 
        { 
            return GetPickup() == null;
        }

        //PRIVATE

        private void SpawnPickup()
        {
            var spawnedPickup = item.SpawnPickup(transform.position, number);
            spawnedPickup.transform.SetParent(transform);
        }

        private void DestroyPickup()
        {
            if (GetPickup())
            {
                Destroy(GetPickup().gameObject);
            }
        }

        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(IsCollected());
        }

        public void RestoreFromJToken(JToken state)
        {
            //This function was slightly modified, in case of bugs check the package code again.
            bool wasCollected = state.ToObject<bool>();

            if (wasCollected)
            {
                DestroyPickup();
            }
        }
    }
}