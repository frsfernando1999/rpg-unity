﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameDevTV.UI
{
    public class ShowHideUI : MonoBehaviour, PlayerInputs.IInventoryActions
    {
        [SerializeField] private GameObject inventoryContainer;
        [SerializeField] private GameObject questContainer;
        private PlayerInputs _controls;

        private void Start()
        {
            questContainer.SetActive(false);
            inventoryContainer.SetActive(false);
        }

        public void OnEnable()
        {
            if (_controls == null)
            {
                _controls = new PlayerInputs();
                _controls.Inventory.SetCallbacks(this);
            }
            _controls.Inventory.Enable();
        }
        
        public void OnDisable()
        {
            _controls?.Inventory.Disable();
        }
        
        public void OnOpenInventory(InputAction.CallbackContext context)
        {
            if (context.performed) { inventoryContainer.SetActive(!inventoryContainer.activeSelf); }
        }

        public void OnOpenQuests(InputAction.CallbackContext context)
        {
            if (context.performed) { questContainer.SetActive(!questContainer.activeSelf); }
        }
    }
}