﻿using TMPro;
using UnityEngine;

namespace RPG.Stats
{
    public class LevelDisplay: MonoBehaviour
    {
        private BaseStats _baseStats;
        private TMP_Text _text;

        private void Awake()
        {
            _baseStats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
            _text = GetComponent<TMP_Text>();
        }

        private void Update()   
        {
            if (_text != null)
            {
                _text.text = $"Level: {_baseStats.GetLevel()}";
            }
        }
    }
}