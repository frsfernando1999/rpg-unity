﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Stats
{
    [CreateAssetMenu(fileName = "Progression", menuName = "Stats/New Progression", order = 0)]
    public class Progression : ScriptableObject
    {
        [SerializeField] ProgressionCharacterClass[] characterClasses = CreateFileWithClasses();

        private static ProgressionCharacterClass[] CreateFileWithClasses()
        {
            ProgressionCharacterClass[] classes = new ProgressionCharacterClass[Enum.GetNames(typeof(CharacterClass)).Length];
            for (int i = 0; i < Enum.GetNames(typeof(CharacterClass)).Length; i++)
            {
                ProgressionCharacterClass characterClass = new ProgressionCharacterClass(Enum.GetName(typeof(CharacterClass), i), (CharacterClass)i);
                classes.SetValue(characterClass, i);
            }
            return classes;
        }

        private Dictionary<CharacterClass, Dictionary<Stat, int[]>> _lookupTable;

        public int GetStat(Stat stat, CharacterClass characterClass, int level)
        {
            BuildLookup();

            int[] levels = _lookupTable[characterClass][stat];

            if (levels.Length < level)
            {
                return 0;
            }

            return levels[level - 1];
        }

        public int GetLevels(Stat stat, CharacterClass characterClass)
        {
            BuildLookup();

            return _lookupTable[characterClass][stat].Length;
        }

        private void BuildLookup()
        {
            if (_lookupTable != null) return;

            _lookupTable = new Dictionary<CharacterClass, Dictionary<Stat, int[]>>();

            foreach (ProgressionCharacterClass progressionClass in characterClasses)
            {
                var statLookupTable = new Dictionary<Stat, int[]>();

                foreach (ProgressionStat progressionStat in progressionClass.stats)
                {
                    statLookupTable[progressionStat.stat] = progressionStat.levels;
                }

                _lookupTable[progressionClass.characterClass] = statLookupTable;
            }
        }

        [Serializable]
        class ProgressionCharacterClass
        {
            [SerializeField] [HideInInspector] private string fontName;
            public CharacterClass characterClass;
            public ProgressionStat[] stats = CreateStats();

            private static ProgressionStat[] CreateStats()
            {
                ProgressionStat[] progressionStats = new ProgressionStat[Enum.GetNames(typeof(Stat)).Length];
                
                for (int i = 0; i < Enum.GetNames(typeof(Stat)).Length; i++)
                {
                    ProgressionStat stat = new ProgressionStat(Enum.GetName(typeof(Stat), i), (Stat)i);
                    progressionStats.SetValue(stat, i);
                }
                return progressionStats;
            }

            public ProgressionCharacterClass(string fontName, CharacterClass characterClass)
            {
                this.fontName = fontName;
                this.characterClass = characterClass;
            }
        }

        [Serializable]
        class ProgressionStat
        {
            [HideInInspector] public string fontName;
            public Stat stat;
            public int[] levels;

            public ProgressionStat(string fontName, Stat stat)
            {
                this.fontName = fontName;
                this.stat = stat;
            }
        }
    }
}