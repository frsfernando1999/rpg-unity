using System;
using GameDevTV.Utils;
using UnityEngine;

namespace RPG.Stats
{
    public class BaseStats : MonoBehaviour
    {
        [Range(1, 50)] [SerializeField] private int level = 1;
        [SerializeField] private CharacterClass characterClass;
        [SerializeField] private Progression progression;
        [SerializeField] private GameObject levelUpParticles;
        [SerializeField] private float destroyParticlesTime = 3f;
        [SerializeField] private bool shouldUseModifiers;
        private Experience _experience;
        private LazyValue<int> _currentLevel;
        
        public event Action OnLevelUp;

        private void Awake()
        {
            _experience = GetComponent<Experience>();
            _currentLevel = new LazyValue<int>(CalculateLevel);
        }

        private void Start()
        {
            _currentLevel.value = CalculateLevel();
        }

        private void OnEnable()
        {
            if (_experience != null)
            {
                _experience.OnExperienceGained += UpdateLevel;
            }
        }

        private void OnDisable()
        {
            if (_experience != null)
            {
                _experience.OnExperienceGained -= UpdateLevel;
            }
        }

        private void UpdateLevel()
        {
            int newLevel = CalculateLevel();
            if (newLevel > _currentLevel.value)
            {
                _currentLevel.value = newLevel;
                if (levelUpParticles != null)
                {
                    GameObject particles = Instantiate(levelUpParticles, transform);
                    Destroy(particles, destroyParticlesTime);
                }
                OnLevelUp?.Invoke();
            }
        }
        public int GetStat(Stat stat)
        {
            return Mathf.RoundToInt((GetBaseStat(stat) +
                                     GetAdditiveModifier(stat)) *
                                    (1 + GetPercentageModifier(stat) / 100));
        }

        private int GetBaseStat(Stat stat)
        {
            return progression.GetStat(stat, characterClass, GetLevel());
        }

        private float GetPercentageModifier(Stat stat)
        {
            if (!shouldUseModifiers) return 0;
            float modifierTotalValue = 0;
            foreach (IModifierProvider modifiers in GetComponents<IModifierProvider>())
            {
                foreach (var modifierValue in modifiers.GetPercentageModifiers(stat))
                {
                    modifierTotalValue += modifierValue;
                }
            }
            return modifierTotalValue;
        }

        private int GetAdditiveModifier(Stat stat)
        {
            if (!shouldUseModifiers) return 0;
            int modifierTotalValue = 0;
            foreach (IModifierProvider modifiers in GetComponents<IModifierProvider>())
            {
                foreach (var modifierValue in modifiers.GetAdditiveModifiers(stat))
                {
                    modifierTotalValue += modifierValue;
                }
            }
            return modifierTotalValue;
        }

        public int GetLevel()
        {
            if (gameObject.CompareTag("Enemy")) return level;
            return _currentLevel.value;
        }
        
        private int CalculateLevel()
        {
            if (_experience == null) return level;

            int currentExp = _experience.ExperiencePoints;
            int penultimateLevel = progression.GetLevels(Stat.ExperienceToLevelUp, characterClass);

            for (int lv = 1; lv <= penultimateLevel; lv++)
            {
                int expToLevelUp = progression.GetStat(Stat.ExperienceToLevelUp, characterClass, lv);
                if (expToLevelUp > currentExp)
                {
                    return lv;
                }
            }
            return penultimateLevel + 1;
        }
    }
}