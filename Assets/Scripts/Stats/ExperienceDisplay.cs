﻿using TMPro;
using UnityEngine;

namespace RPG.Stats
{
    public class ExperienceDisplay: MonoBehaviour
    {
        private Experience _experience;
        private TMP_Text _text;

        private void Awake()
        {
            _experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
            _text = GetComponent<TMP_Text>();
        }

        private void Update()   
        {
            if (_text != null)
            {
                _text.text = $"Experience: {_experience.ExperiencePoints}";
            }
        }
    }
}