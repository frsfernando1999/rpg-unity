﻿using System;
using Newtonsoft.Json.Linq;
using RPG.Saving.Json;
using UnityEngine;

namespace RPG.Stats
{
    public class Experience : MonoBehaviour, IJsonSaveable
    {
        [SerializeField] private int experiencePoints = 0;

        /*
        public delegate void ExperienceGainedDelegate();
        public event ExperienceGainedDelegate onExperienceGained;
        */
        //The line below represents both lines above
        public event Action OnExperienceGained;

        public int ExperiencePoints => experiencePoints;

        public void GainExperience(int experience)
        {
            experiencePoints += experience;
            OnExperienceGained?.Invoke();
        }
        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(experiencePoints);
        }

        public void RestoreFromJToken(JToken state)
        {
            int experience = state.ToObject<int>();
            experiencePoints = experience;
        }
    }
}