﻿namespace RPG.Stats
{
    public enum CharacterClass
    {
        Player,
        Grunt,
        Swordsman,
        Mage,
        Archer
    }
}