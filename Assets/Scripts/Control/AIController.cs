using GameDevTV.Utils;
using Newtonsoft.Json.Linq;
using RPG.Attributes;
using RPG.Combat;
using RPG.Core;
using RPG.Movement;
using RPG.Saving.Json;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RPG.Control
{
    public class AIController : MonoBehaviour, IJsonSaveable
    {
        [SerializeField] private float chaseDistance = 5f;
        [SerializeField] private float aggroNearbyMobsDistance = 5f;
        [SerializeField] private float suspicionTime = 5f;
        [SerializeField] private PatrolPath patrolPath;
        [SerializeField] private float waypointTolerance = 1f;
        [SerializeField] private float waypointDwellTimeMin = 3f;
        [SerializeField] private float aggravatedTime = 5f;
        [SerializeField] private float waypointDwellTimeMax = 10f;
        [SerializeField] [Range(0, 1)] private float patrolSpeedFactor = 0.5f;
        
        private GameObject _player;
        private Fighter _fighter;
        private Mover _mover;
        private Health _health;
        private ActionScheduler _actionScheduler;

        private LazyValue<Vector3> _guardPosition;
        private float _timeSinceLastSawPlayer = Mathf.Infinity;
        private float _timeSincePlayerLastAttacked = Mathf.Infinity;
        private float _timeOnWaypoint = Mathf.Infinity;
        private int _currentWaypoint;
        private float _currentDwellingWaypointTime;

        private void Awake()
        {
            _guardPosition = new LazyValue<Vector3>(GetGuardInitialPosition);
            SetDependencies();
        }

        private Vector3 GetGuardInitialPosition()
        {
            return transform.position;
        }

        private void Start()
        {
            _guardPosition.ForceInit();
        }
        
        private void Update()
        {
            if (_health.GetIsDead()) return;
            _timeSincePlayerLastAttacked += Time.deltaTime;
            Chase();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(gameObject.transform.position, chaseDistance);
        }
        
        private void SetDependencies()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            _fighter = GetComponent<Fighter>();
            _mover = GetComponent<Mover>();
            _actionScheduler = GetComponent<ActionScheduler>();
            _health = GetComponent<Health>();
        }
        
        private void Chase()
        {
            
            if (IsAggravated())
            {
                _timeSinceLastSawPlayer = 0;
                AttackBehavior();
            }
            else if (_timeSinceLastSawPlayer < suspicionTime)
            {
                SuspicionBehavior();
            }
            else
            {
                PatrolBehavior();
            }
            _timeSinceLastSawPlayer += Time.deltaTime;
        }

        private bool IsAggravated()
        {
            float distanceToPlayer = Vector3.Distance(_player.transform.position, transform.position);
            return distanceToPlayer < chaseDistance || _timeSincePlayerLastAttacked < aggravatedTime;
        }

        public void Aggravate()
        {
            _timeSincePlayerLastAttacked = 0f;
        }

        private void PatrolBehavior()
        {
            Vector3 nextPosition = _guardPosition.value;

            if (patrolPath != null)
            {
                if (AtWaypoint())
                {
                    _currentDwellingWaypointTime = Random.Range(waypointDwellTimeMin, waypointDwellTimeMax);
                    _timeOnWaypoint = 0;
                    CycleWaypoint();
                }
                
                nextPosition = GetCurrentWaypoint();
            }

            if (_timeOnWaypoint > _currentDwellingWaypointTime)
            {
                _mover.StartMoveAction(nextPosition, patrolSpeedFactor);
            }
        }

        private Vector3 GetCurrentWaypoint()
        {
            return patrolPath.GetPosition(_currentWaypoint);
        }

        private void CycleWaypoint()
        {
            _currentWaypoint = patrolPath.GetNextIndex(_currentWaypoint);
        }

        private bool AtWaypoint()
        {
            _timeOnWaypoint += Time.deltaTime;
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void SuspicionBehavior()
        {
            _actionScheduler.CancelCurrentAction();
        }

        private void AttackBehavior()
        {
            _fighter.Attack(_player);
            AggravateNearbyEnemies();
        }

        private void AggravateNearbyEnemies()
        {
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, aggroNearbyMobsDistance, Vector3.up, 0);

            foreach (RaycastHit hit in hits)
            {
                AIController enemy = hit.transform.GetComponent<AIController>();
                if (enemy != null)
                {
                    enemy.Aggravate();
                }
            }
            
        }

        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(_guardPosition.value.ToToken());
        }

        public void RestoreFromJToken(JToken state)
        {
            _guardPosition.value = state.ToVector3();
        }
    }
}