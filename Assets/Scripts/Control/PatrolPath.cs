using UnityEngine;

namespace RPG.Control
{
    public class PatrolPath : MonoBehaviour
    {
        [SerializeField] private float sphereRadius = 0.15f;
        private void OnDrawGizmos()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                int j = GetNextIndex(i);
                Gizmos.DrawSphere(GetPosition(i), sphereRadius);
                Gizmos.DrawLine(GetPosition(i), GetPosition(j));
            }
        }

        public int GetNextIndex(int i)
        {
            if (i + 1 >= transform.childCount)
            {
                return 0;
            }
            return i + 1;
            
        }

        public Vector3 GetPosition(int i)
        {
            return transform.GetChild(i).position;
        }
    }
}