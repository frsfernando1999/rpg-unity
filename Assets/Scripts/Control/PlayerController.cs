using System;
using GameDevTV.Inventories;
using RPG.Attributes;
using RPG.Movement;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace RPG.Control
{
    public class PlayerController : MonoBehaviour, PlayerInputs.IActionBarActions
    {
        private Mover _mover;
        private Camera _camera;
        private Health _health;
        private ActionStore _actionStore;
        private CursorType _currentCursor;

        [SerializeField] private float navNavMeshProjectionDistance = 0.5f;
        [SerializeField] private float raycastRadius = 0.3f;

        private bool _isDraggingUI;

        public Mover Mover => _mover;

        [SerializeField] private CursorMapping[] cursorMappings;
        
        private PlayerInputs _controls;


        [Serializable]
        struct CursorMapping
        {
            public CursorType cursorType;
            public Texture2D texture;
            public Vector2 hotspot;
        }

        private void OnEnable()
        {
            if (_controls == null)
            {
                _controls = new PlayerInputs();
                _controls.ActionBar.SetCallbacks(this);
            }
            _controls.ActionBar.Enable();

        }

        private void OnDisable()
        {
            _controls.ActionBar.Disable();
        }

        private CursorMapping GetCursorMapping(CursorType type)
        {
            foreach (CursorMapping mapping in cursorMappings)
            {
                if (mapping.cursorType == type)
                {
                    return mapping;
                }
            }

            return cursorMappings[0];
        }

        private void Awake()
        {
            SetDependencies();
        }

        private void SetDependencies()
        {
            _actionStore = GetComponent<ActionStore>();
            _mover = GetComponent<Mover>();
            _health = GetComponent<Health>();
            _camera = Camera.main;
        }


        void Update()
        {
            if (InteractWithUI()) return;
            if (_health.GetIsDead())
            {
                SetCursor(CursorType.None);
                return;
            }

            if (InteractWithComponent()) return;
            if (InteractWithMovement()) return;
            
            SetCursor(CursorType.None);
        }

        private bool InteractWithUI()
        {
            if (Input.GetMouseButtonUp(0))
            {
                _isDraggingUI = false;
            }
            if (EventSystem.current.IsPointerOverGameObject())
            {
                if (Input.GetMouseButtonDown(0))
                {
                    _isDraggingUI = true;
                }
                SetCursor(CursorType.UI);
                return true;
            }
            return _isDraggingUI;
        }

        private bool InteractWithComponent()
        {
            RaycastHit[] hits = RaycastAllSorted();
            foreach (RaycastHit hit in hits)
            {
                IRaycastable[] raycastables = hit.transform.GetComponents<IRaycastable>();
                foreach (IRaycastable raycastable in raycastables)
                {
                    if (raycastable.HandleRaycast(this))
                    {
                        SetCursor(raycastable.GetCursorType());
                        return true;
                    }
                }
            }

            return false;
        }

        private RaycastHit[] RaycastAllSorted()
        {
            RaycastHit[] results = Physics.SphereCastAll(GetMouseRay(), raycastRadius);

            float[] distances = new float[results.Length];
            for (int i = 0; i < distances.Length; i++)
            {
                distances[i] = results[i].distance;
            }

            Array.Sort(distances, results);
            return results;
        }

        private bool InteractWithMovement()
        {
            if (_camera == null) return false;
            if (!RaycastNavMesh(out var target)) return false;
            if (!_mover.CanMoveTo(target)) return false; 
            if (Input.GetMouseButton(0)) _mover.StartMoveAction(target, 1f);
            SetCursor(CursorType.Movement);
            return true;
        }

        private bool RaycastNavMesh(out Vector3 target)
        {
            target = new Vector3();
            bool raycastHit = Physics.Raycast(GetMouseRay(), out RaycastHit hit);
            if (!raycastHit) return false;
            bool validPoint = NavMesh.SamplePosition(hit.point, out var meshHit, navNavMeshProjectionDistance, NavMesh.AllAreas);
            if (!validPoint) return false;
            target = meshHit.position;

            return true;
        }

        private void SetCursor(CursorType type)
        {
            if (_currentCursor == type) return;
            CursorMapping mapping = GetCursorMapping(type);
            Cursor.SetCursor(mapping.texture, mapping.hotspot, CursorMode.Auto);
            _currentCursor = type;
        }

        private Ray GetMouseRay()
        {
            return _camera.ScreenPointToRay(Input.mousePosition);
        }

        public void OnAction1(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _actionStore.Use(0, gameObject);
            }
        }

        public void OnAction2(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _actionStore.Use(1, gameObject);
            }
        }

        public void OnAction3(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _actionStore.Use(2, gameObject);
            }
        }

        public void OnAction4(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _actionStore.Use(3, gameObject);
            }
        }

        public void OnAction5(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _actionStore.Use(4, gameObject);
            }
        }

        public void OnAction6(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _actionStore.Use(5, gameObject);
            }
        }
    }
}