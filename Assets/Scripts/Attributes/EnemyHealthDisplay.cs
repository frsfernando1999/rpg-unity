﻿using RPG.Combat;
using TMPro;
using UnityEngine;

namespace RPG.Attributes
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        private Health _health;
        private TMP_Text _text;
        private Fighter _player;

        private void Awake()
        {
            _player = GameObject.FindWithTag("Player").GetComponent<Fighter>();
            _text = GetComponent<TMP_Text>();
        }

        private void Update()
        {
            _health = _player.GetTarget();
            _text.text = _health == null ? "Enemy HP: N/A" : $"Health: {_health.CurrentHealth}/{_health.MaxHealth}";
        }
    }
}