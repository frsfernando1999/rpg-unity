using System;
using GameDevTV.Utils;
using Newtonsoft.Json.Linq;
using RPG.Core;
using RPG.Inventories;
using RPG.Saving.Json;
using RPG.Stats;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Attributes
{
    public class Health : MonoBehaviour, IJsonSaveable
    {
        private LazyValue<float> _maxHealth;

        public float CurrentHealth { get; private set; }
        private bool _inventoryLoaded;
        public float MaxHealth => _maxHealth.value;

        private bool _isDead;
        private BaseStats _baseStats;

        [SerializeField] private UnityEvent<float> takeDamage;
        [SerializeField] private UnityEvent onDie;

        private StatsEquipment _equipment;

        public bool GetIsDead() => _isDead;

        private static readonly int IsDead = Animator.StringToHash("isDead");

        private void Awake()
        {
            _baseStats = GetComponent<BaseStats>();
            _equipment = GetComponent<StatsEquipment>();
            
            _maxHealth = new LazyValue<float>(GetInitialHealth);
            CurrentHealth = _maxHealth.value;
        }

        private void Start()
        {
            _inventoryLoaded = true;
            _maxHealth.ForceInit();
        }

        private void OnEnable()
        {
            _baseStats.OnLevelUp += OnLevelUp;
            if (_equipment) _equipment.EquipmentUpdated += HandleEquipmentUpdated;
        }

        private void OnDisable()
        {
            _baseStats.OnLevelUp -= OnLevelUp;
            if (_equipment) _equipment.EquipmentUpdated -= HandleEquipmentUpdated;
        }
        
        private void HandleEquipmentUpdated()
        {
            if (_inventoryLoaded == false) return;
            _maxHealth.value = _baseStats.GetStat(Stat.Health);
            if (CurrentHealth > _maxHealth.value)
            {
                CurrentHealth = _maxHealth.value;
            }
        }
        
        private float GetInitialHealth()
        {
            return _baseStats.GetStat(Stat.Health);
        }
        
        private void OnLevelUp()
        {
            float oldMaxHealth = _maxHealth.value;
            _maxHealth.value = _baseStats.GetStat(Stat.Health);
            
            float levelUpHpDifference = _maxHealth.value - oldMaxHealth;
            
            CurrentHealth += levelUpHpDifference;
        }

        public void TakeDamage(GameObject instigator, float damage)
        {
            takeDamage.Invoke(damage);
            CurrentHealth = Mathf.Clamp(CurrentHealth - damage, 0, _maxHealth.value);
            if (CurrentHealth <= 0 && !_isDead)
            {
                onDie.Invoke(); 
                Die(instigator);
            };
        }

        private void Die(GameObject instigator)
        {
            if (instigator != null && instigator.CompareTag("Player"))
            {
                instigator.GetComponent<Experience>().GainExperience(_baseStats.GetStat(Stat.ExperienceReward));
            }
            _isDead = true;
            GetComponent<Animator>().SetTrigger(IsDead);
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        [Serializable]
        private struct HealthData
        {
            public float health;
            public float maxHealth;

            public HealthData(float health, float maxHealth)
            {
                this.health = health;
                this.maxHealth = maxHealth;
            }
        }
        
        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(new HealthData(CurrentHealth, _maxHealth.value));
        }

        public void RestoreFromJToken(JToken state)
        {
            HealthData healthData = state.ToObject<HealthData>();
            CurrentHealth = healthData.health;
            _maxHealth.value = healthData.maxHealth;
            if (CurrentHealth <= 0) Die(null);
        }

        public float GetHealthPercentage()
        {
            return CurrentHealth / _maxHealth.value;
        }

        public void Heal(int healthToRestore)
        {
            CurrentHealth = Mathf.Clamp(CurrentHealth + healthToRestore, 0f, _maxHealth.value);
        }
    }
}