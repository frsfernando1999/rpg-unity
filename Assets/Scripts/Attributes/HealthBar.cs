using UnityEngine;

namespace RPG.Attributes
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Health healthScript;
        [SerializeField] private RectTransform healthBarTransform;
        [SerializeField] private Canvas canvas;

        private void Update()
        {
            if (healthScript == null || canvas == null) return;
               if (Mathf.Approximately(healthScript.GetHealthPercentage(), 0) || Mathf.Approximately(healthScript.GetHealthPercentage(), 1))
               {
                   canvas.enabled = false;
                   return;
               }
               canvas.enabled = true;
               healthBarTransform.localScale = new Vector3(healthScript.GetHealthPercentage(), 1, 1);
        }
    }
}