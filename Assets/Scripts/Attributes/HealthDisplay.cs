﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RPG.Attributes
{
    public class HealthDisplay: MonoBehaviour
    {
        private Health _health;
        private TMP_Text _text;

        private void Awake()
        {
            _health = GameObject.FindWithTag("Player").GetComponent<Health>();
            _text = GetComponent<TMP_Text>();
        }

        private void Update()   
        {
            if (_text != null)
            {
                _text.text = $"HP: {_health.CurrentHealth}/{_health.MaxHealth}";
            }
        }
    }
}