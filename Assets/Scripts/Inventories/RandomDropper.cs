﻿using GameDevTV.Inventories;
using RPG.Stats;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Inventories
{
    public class RandomDropper : ItemDropper
    {
        [Tooltip("How far can the pickups be scattered from the dropper")] [SerializeField]
        private float scatterDistance = 1.5f;

        [SerializeField] private InventoryItem[] dropLibrary;
        // [SerializeField] private DropLibrary dropLibrary;

        [SerializeField] private int maxStackDrop = 3;
        [SerializeField] private int minStackDrop = 1;
        [SerializeField] private int maxDropNumber = 4;
        [SerializeField] private int minDropNumber = 0;

        private const int ATTEMPTS = 20;

        public void RandomDrop()
        {
            //if (dropLibrary.Length == 0) return;
            
            // Uses DropLibrary Scriptable Object
            // has a better probability calculation*
            
            // var baseStats = GetComponent<BaseStats>();
            // var drops = dropLibrary.GetRandomDrops(baseStats.GetLevel());
            // foreach (var drop in drops)
            // {
            //     DropItem(drop.item, drop.number);
            // }


            //Uses this script with an array of drops
            // has simple probability calculation*

            for (int i = 0; i < Random.Range(minDropNumber, maxDropNumber); i++)
            {
                int randomNumber = Random.Range(0, dropLibrary.Length);
                var item = dropLibrary[randomNumber];
                if (item.IsStackable())
                {
                    DropItem(item, Random.Range(minStackDrop, maxStackDrop));
                }
                else
                {
                    DropItem(item, 1);
                }
            }
        }

        protected override Vector3 GetDropLocation()
        {
            for (int i = 0; i < ATTEMPTS; i++)
            {
                Vector3 randomPoint = transform.position + Random.insideUnitSphere * scatterDistance;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 0.1f, NavMesh.AllAreas))
                {
                    return hit.position;
                }
            }

            return transform.position;
        }
    }
}