using GameDevTV.Inventories;
using UnityEngine;

namespace RPG.Inventories.ActionItems
{
    [CreateAssetMenu(menuName = ("Inventory/Consumables/Mana Potion"))]
    public class ManaPotion : ActionItem
    {
        public override void Use(GameObject user)
        {
            Debug.Log("Using Mana Potion");
        }
    }
}