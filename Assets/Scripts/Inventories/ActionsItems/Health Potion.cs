using GameDevTV.Inventories;
using RPG.Attributes;
using UnityEngine;

namespace RPG.Inventories.ActionItems
{
    [CreateAssetMenu(menuName = "Inventory/Consumables/Health Potion")]
    public class HealthPotion : ActionItem
    {
        [SerializeField] private int healthToRestore = 50;
        public override void Use(GameObject user)
        {
            Health health = user.GetComponent<Health>();
            if (health != null)
            {
                health.Heal(healthToRestore);
            }
        }
    }
}