using Newtonsoft.Json.Linq;
using RPG.Attributes;
using RPG.Core;
using RPG.Saving.Json;
using UnityEngine;
using UnityEngine.AI;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction, IJsonSaveable
    {

        [SerializeField] private float maxSpeed = 5;
        [SerializeField] private float maxNavPathLength = 20f;

        
        private NavMeshAgent _navMeshAgent;
        private Animator _playerAnimator;
        private ActionScheduler _actionScheduler;
        private Health _health;

        private static readonly int Move = Animator.StringToHash("Move");

        private void Awake()
        {
            SetDependencies();
        }

        private void Update()
        {
            _navMeshAgent.enabled = !_health.GetIsDead();
            _playerAnimator.SetFloat(Move, _navMeshAgent.velocity.magnitude);
        }

        private void SetDependencies()
        {
            _playerAnimator = GetComponent<Animator>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _actionScheduler = GetComponent<ActionScheduler>();
            _health = GetComponent<Health>();
        }

        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            if (_health.GetIsDead()) return;
            _actionScheduler.StartAction(this);
            MoveTo(destination, speedFraction);
        }

        public bool CanMoveTo(Vector3 destination)
        {
            NavMeshPath path = new NavMeshPath();
            bool hasPath = NavMesh.CalculatePath(transform.position, destination, NavMesh.AllAreas, path);
            if (!hasPath || path.status != NavMeshPathStatus.PathComplete) return false;
            if (GetPathLength(path) > maxNavPathLength) return false;
            return true;
        }
        
        private float GetPathLength(NavMeshPath path)
        {
            float distance = 0f;
            if (path.corners.Length < 2) return distance;
            
            Vector3 previousCorner = transform.position;
            foreach (Vector3 corner in path.corners)
            {
                distance += Vector3.Distance(corner, previousCorner);
                previousCorner = corner;
            }
            return distance;
        }

        public void MoveTo(Vector3 location, float speedFraction)
        {
            _navMeshAgent.SetDestination(location);
            _navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
            _navMeshAgent.isStopped = false;
        }

        public void Cancel()
        {
            _navMeshAgent.isStopped = true;
        }
        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(transform.position.ToToken());
        }

        public void RestoreFromJToken(JToken state)
        {
            _navMeshAgent.enabled = false;
            gameObject.transform.position = state.ToVector3();
            _navMeshAgent.enabled = true;
            _actionScheduler.CancelCurrentAction();
        }
    }
}