﻿using GameDevTV.Inventories;
using GameDevTV.Utils;
using Newtonsoft.Json.Linq;
using RPG.Attributes;
using RPG.Core;
using RPG.Movement;
using RPG.Saving.Json;
using RPG.Stats;
using UnityEngine;

namespace RPG.Combat
{
    public class Fighter : MonoBehaviour, IAction, IJsonSaveable
    {
        [SerializeField] private Transform rightHandTransform ;
        [SerializeField] private Transform leftHandTransform;
        [SerializeField] private WeaponConfig defaultWeapon;
        private WeaponConfig _currentWeaponConfig;
        private LazyValue<WeaponComponent> _currentWeapon;
    

        private float _timeSinceLastAttack = Mathf.Infinity;

        private Health _target;
        private Equipment _equipment;
        
        private Mover _mover;
        private ActionScheduler _actionScheduler;
        private Animator _animator;
        private BaseStats _baseStats;
        
        public Health GetTarget() => _target;

        private static readonly int Attacking = Animator.StringToHash("Attacking");
        private static readonly int CancelAttack = Animator.StringToHash("CancelAttack");

        private void Awake()
        {
            _currentWeaponConfig = defaultWeapon;
            _currentWeapon = new LazyValue<WeaponComponent>(SetupDefaultWeapon);

            _equipment = GetComponent<Equipment>();
            if (_equipment)
            {
                _equipment.EquipmentUpdated += UpdateWeapon;
            }
            SetDependencies();
        }

        private void UpdateWeapon()
        {
            WeaponConfig weapon = _equipment.GetItemInSlot(EquipLocation.Weapon) as WeaponConfig;
            if(weapon != null)
            {   
                EquipWeapon(weapon);
            }
            else
            {
                EquipWeapon(defaultWeapon);
            }
        }

        private WeaponComponent SetupDefaultWeapon()
        {
            return AttachWeapon(defaultWeapon);
        }

        private void Start()
        {
            _currentWeapon.ForceInit();
        }

        private void Update()
        {
            _timeSinceLastAttack += Time.deltaTime;
            if (_target == null || _target.GetIsDead()) return;

            if (GetIsInRange(_target.transform))
            {
                _mover.Cancel();
                AttackBehavior();
            }
            else
            {
                _mover.MoveTo(_target.transform.position, 1f);
            }
        }

        public bool GetIsInRange(Transform targetPosition)
        {
            return Vector3.Distance(targetPosition.position, transform.position) < _currentWeaponConfig.WeaponRange;
        }

        private void SetDependencies()
        {
            _mover = GetComponent<Mover>();
            _actionScheduler = GetComponent<ActionScheduler>();
            _animator = GetComponent<Animator>();
            _baseStats = GetComponent<BaseStats>();
        }
        public void EquipWeapon(WeaponConfig weapon)
        {
            _currentWeaponConfig = weapon;
            _currentWeapon.value = AttachWeapon(weapon);
        }

        private WeaponComponent AttachWeapon(WeaponConfig weapon)
        {
            if (weapon == null || rightHandTransform == null) return null;
            return weapon.SpawnWeapon(rightHandTransform, leftHandTransform, _animator);
        }

        private void AttackBehavior()
        {
            transform.LookAt(_target.transform);
            if (_timeSinceLastAttack > _currentWeaponConfig.AttackSpeed)
            {
                _animator.ResetTrigger(CancelAttack);
                _animator.SetTrigger(Attacking);
                _timeSinceLastAttack = 0f;
            }
        }

        public void Attack(GameObject combatTarget)
        {
            _target = combatTarget.GetComponent<Health>();
            if (_target == null) return;
            _actionScheduler.StartAction(this);
        }

        public void Cancel()
        {
            _animator.ResetTrigger(Attacking);
            _animator.SetTrigger(CancelAttack);
            _target = null;
            _mover.Cancel();
        }

        //Animation Event
        private void Hit()
        {
            if (_target == null) return;
            if (_currentWeaponConfig == defaultWeapon)
            {
                _target.TakeDamage(gameObject, defaultWeapon.Damage);
            }
            else
            {
                _target.TakeDamage(gameObject, _baseStats.GetStat(Stat.Damage));
            }

            if (_currentWeapon.value != null)
            {
                _currentWeapon.value.OnHit();
            }
        }
        private void Shoot()
        {
            if (_currentWeaponConfig.HasProjectile())
            {
                _currentWeaponConfig.LaunchProjectile(leftHandTransform, _target, gameObject, _baseStats.GetStat(Stat.Damage));
            }
        }
        
        //Saving

        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(_currentWeaponConfig.name);
        }

        public void RestoreFromJToken(JToken state)
        {
            string weaponName = state.ToObject<string>();
            WeaponConfig weapon = Resources.Load<WeaponConfig>(weaponName);
            EquipWeapon(weapon);
        }

    }
}