using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    public class WeaponComponent : MonoBehaviour
    {
        [SerializeField] private UnityEvent onHit;
        public void OnHit()
        {
            onHit.Invoke();
        }
    }
}