using Game.Combat;
using UnityEngine;

namespace RPG.Combat
{
    public class AggroGroup : MonoBehaviour
    {
        [SerializeField] private Fighter[] fighters;
        [SerializeField] private bool activateOnStart;

        public void Start()
        {
           Activate(activateOnStart); 
        }

        public void Activate(bool bShouldActivate)
        {
            foreach (Fighter fighter in fighters)
            {
                CombatTarget target = fighter.gameObject.GetComponent<CombatTarget>();
                if (target != null)
                {
                    target.enabled = bShouldActivate;
                }
                fighter.enabled = bShouldActivate;
            }
        }
    }
}