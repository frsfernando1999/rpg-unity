﻿using System;
using System.Collections.Generic;
using GameDevTV.Inventories;
using RPG.Attributes;
using RPG.Stats;
using UnityEngine;

namespace RPG.Combat
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Make New Weapon", order = 0)]
    public class WeaponConfig : EquipableItem, IModifierProvider
    {
        [Header("Weapon Configuration")] [SerializeField]
        private AnimatorOverrideController animationOverride;
        [SerializeField] private WeaponComponent weaponPrefab;
        [SerializeField] private Projectile projectile;

        [Header("Weapon Attributes")] [SerializeField]
        private float weaponRange = 2f;

        [SerializeField] private float attackSpeed = 1.25f;
        [SerializeField] private int damage = 5;
        [SerializeField] private float percentageModifier = 10f;
        [SerializeField] private bool isRightHanded = true;

        private const string WeaponName = "Weapon";

        public float WeaponRange => weaponRange;
        public float AttackSpeed => attackSpeed;
        public int Damage => damage;
        public float PercentageModifier => percentageModifier;

        public WeaponComponent SpawnWeapon(Transform rightHand, Transform leftHand, Animator animator)
        {
            if (weaponPrefab == null) return null;

            DestroyOldWeapon(rightHand, leftHand);
            WeaponComponent weapon = Instantiate(weaponPrefab, isRightHanded ? rightHand : leftHand);
            weapon.name = WeaponName;
            if (animationOverride != null)
            {
                animator.runtimeAnimatorController = animationOverride;
            }
            else
            {
                var overrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
                if (overrideController != null)
                {
                    animator.runtimeAnimatorController = overrideController.runtimeAnimatorController;
                }
            }

            return weapon;
        }

        private void DestroyOldWeapon(Transform rightHand, Transform leftHand)
        {
            Transform oldWeapon = rightHand.Find(WeaponName);
            if (oldWeapon == null)
            {
                oldWeapon = leftHand.Find(WeaponName);
            }

            if (oldWeapon == null) return;
            oldWeapon.name = "DESTROYING";
            Destroy(oldWeapon.gameObject);
        }

        public bool HasProjectile() => projectile != null;

        public void LaunchProjectile(Transform leftHandTransform, Health target, GameObject instigator,
            float calculatedDamage)
        {
            if (target == null) return;
            Projectile projectileInstance = Instantiate(projectile, leftHandTransform.position, Quaternion.identity);
            projectileInstance.SetTarget(target, instigator, calculatedDamage);
        }

        public IEnumerable<int> GetAdditiveModifiers(Stat stat)
        {
            if (stat == Stat.Damage)
            {
                yield return damage;
            }
        }

        public IEnumerable<float> GetPercentageModifiers(Stat stat)
        {
            if (stat == Stat.Damage)
            {
                yield return percentageModifier;
            }
        }
    }
}