﻿using RPG.Attributes;
using RPG.Combat;
using RPG.Control;
using RPG.Movement;
using UnityEngine;

namespace Game.Combat
{
    [RequireComponent(typeof(Health))]
    public class CombatTarget : MonoBehaviour, IRaycastable
    {
        private Health _health;
        private void Awake()
        {
            _health = GetComponent<Health>();
        }

        private bool CanAttack()
        {
            if (_health == null) return false;
            return !_health.GetIsDead();
            
        }

        public CursorType GetCursorType()
        {
            return CursorType.Combat;
        }

        public bool HandleRaycast(PlayerController callingController)
        {
            if (!enabled) return false;
            if (!CanAttack()) return false;
            if (!callingController.GetComponent<Mover>().CanMoveTo(transform.position) 
                && !callingController.GetComponent<Fighter>().GetIsInRange(transform)) return false;
            if (Input.GetMouseButtonDown(0))
            {
                callingController.GetComponent<Fighter>().Attack(gameObject);
            }
            return true;
        }
    }
}