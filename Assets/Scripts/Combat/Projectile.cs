using RPG.Attributes;
using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float speed = 1;
        [SerializeField] private float destroyParticlesTime = 1f;
        [SerializeField] private bool isHoming = true;
        [SerializeField] private GameObject hitEffect;
        [SerializeField] private float maxProjectileLifetime = 10f;
        [SerializeField] private float projectileHangAroundTime = 0.1f;
        [SerializeField] private UnityEvent onHit;

        private Health _target;
        private GameObject _instigator;
        private float _damage;

        private void Start()
        {
            SetupProjectileLifetime();
            transform.LookAt(GetAimLocation());
        }

        private void SetupProjectileLifetime()
        {
            if (hitEffect != null)
            {
                Invoke(nameof(PlayDestructionParticles), maxProjectileLifetime);
            }
            Destroy(gameObject, maxProjectileLifetime);
        }

        void Update()
        {
            if (_target == null && _target.GetIsDead()) return;
            if (isHoming)
            {
                if (!_target.GetIsDead())
                {
                    transform.LookAt(GetAimLocation());
                }
            }

            transform.Translate(Vector3.forward * (speed * Time.deltaTime));
        }
        
        public void SetTarget(Health targetH, GameObject instigatorH,float damageH)
        {
            _target = targetH;
            _damage = damageH;
            _instigator = instigatorH;
        }

        private Vector3 GetAimLocation()
        {
            if (_target == null) return Vector3.zero;
            CapsuleCollider targetCapsule = _target.GetComponent<CapsuleCollider>();
            if (targetCapsule == null)
            {
                return _target.transform.position;
            }
            return _target.transform.position + Vector3.up * targetCapsule.height / 2;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Health>() != _target) return;
            if (_target.GetIsDead()) return;
            _target.TakeDamage(_instigator, _damage);
            onHit.Invoke();
            if (hitEffect != null)
            {
                PlayDestructionParticles();
            }
            
            Destroy(gameObject, projectileHangAroundTime);
        }

        private void PlayDestructionParticles()
        {
            GameObject particles = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(particles, destroyParticlesTime);
        }
    }
}