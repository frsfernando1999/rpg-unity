﻿using TMPro;
using UnityEngine;

namespace RPG.UI.Quests
{
    public class RewardUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;

        public TextMeshProUGUI Text => text;
    }
}