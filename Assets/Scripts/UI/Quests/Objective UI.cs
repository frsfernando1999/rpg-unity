using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveUI : MonoBehaviour
{
    [SerializeField] private Image checkmarkBox;
    [SerializeField] private Image checkmark;
    [SerializeField] private TextMeshProUGUI text;

    public Image CheckmarkBox => checkmarkBox;

    public Image Checkmark => checkmark;

    public TextMeshProUGUI Text => text;
}
