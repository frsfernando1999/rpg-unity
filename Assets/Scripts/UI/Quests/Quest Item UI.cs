using System;
using RPG.Quests;
using TMPro;
using UnityEngine;

namespace RPG.UI.Quests
{
    public class QuestItemUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI progress;

        private QuestStatus _quest;

        public void Setup(QuestStatus quest)
        {
            if (quest == null) return;
            
            _quest = quest;
            
            if (title != null) title.text = quest.GetQuest().Title;
            if (progress != null) progress.text = quest.GetCompletedObjectives() + "/" + quest.GetQuest().Progress;
        }

        public QuestStatus GetQuestStatus()
        {
            return _quest;
        }
    }
}