using GameDevTV.Core.UI.Tooltips;
using RPG.Quests;
using UnityEngine;

namespace RPG.UI.Quests
{
    public class QuestTooltipSpawner : TooltipSpawner
    {
        public override void UpdateTooltip(GameObject tooltip)
        {
            if (tooltip == null) return; 

            QuestItemUI questItemUI = GetComponent<QuestItemUI>();
            if (questItemUI == null) return;
            QuestStatus quest = questItemUI.GetQuestStatus();
            if (quest == null) return;
            
            tooltip.GetComponent<QuestTooltipUI>().Setup(quest);

        }

        public override bool CanCreateTooltip()
        {
            return true;
        }
    }
}