using RPG.Quests;
using UnityEngine;

namespace RPG.UI.Quests
{
    public class QuestListUI : MonoBehaviour
    {
        [SerializeField] private QuestItemUI questPrefab;
        private QuestList _questList;

        private void Awake()
        {
            _questList = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestList>();
        }

        private void Start()
        {
            ClearQuestList();
            AddQuests();
        }

        private void OnEnable()
        {
            _questList.UpdatedQuestList += UpdateUI;
            UpdateUI();
        }

        private void OnDisable()
        {
            _questList.UpdatedQuestList -= UpdateUI;
        }

        private void UpdateUI()
        {
            ClearQuestList();
            AddQuests();
        }

        private void ClearQuestList()
        {
            foreach (Transform quest in transform)
            {
                Destroy(quest.gameObject);
            }
        }

        private void AddQuests()
        {
            QuestList questList = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestList>();
            foreach (QuestStatus status in questList.GetStatuses())
            {
                QuestItemUI questInstance = Instantiate(questPrefab, transform);
                questInstance.Setup(status);
            }
        }
    }
}