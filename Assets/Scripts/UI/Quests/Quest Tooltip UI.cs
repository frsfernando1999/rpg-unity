using System.Linq;
using RPG.Quests;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace RPG.UI.Quests
{
    public class QuestTooltipUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private Transform objectivesListRoot;
        [SerializeField] private Transform rewardsListRoot;
        [SerializeField] private ObjectiveUI objectivePrefab;
        [SerializeField] private RewardUI rewardPrefab;

        public void Setup(QuestStatus status)
        {
            Quest quest = status.GetQuest();
            if (title != null)
            {
                title.text = quest.Title;
            }

            CleanObjectives();
            CreateObjectiveList(status);
            CleanRewards();
            CreateRewardsList(status.GetQuest());
        }

        private void CleanRewards()
        {
            foreach (Transform child in rewardsListRoot)
            {
                Destroy(child.gameObject);
            }
        }

        private void CreateRewardsList(Quest quest)
        {
            var rewardsList = quest.GetRewards();
            foreach (var reward in rewardsList)
            {
                var rewardInstance = Instantiate(rewardPrefab, rewardsListRoot);
                if (reward.item == null)
                {
                    rewardInstance.Text.text = "None";
                    return;
                }

                if (reward.amount == 1)
                {
                    rewardInstance.Text.text = "A " + reward.item.GetDisplayName();
                }
                else
                {
                    rewardInstance.Text.text = reward.amount + " " +  reward.item.GetDisplayName() + "s";
                }
            }
        }


        private void CreateObjectiveList(QuestStatus status)
        {
            if (objectivesListRoot != null)
            {
                foreach (var obj in status.GetQuest().Objectives)
                {
                    var instantiatedObj = Instantiate(objectivePrefab, objectivesListRoot);
                    instantiatedObj.Text.text = obj.description;

                    if (status.IsObjectiveComplete(obj.reference))
                    {
                        instantiatedObj.CheckmarkBox.color = Color.green;
                        instantiatedObj.Checkmark.gameObject.SetActive(true);
                    }
                }
            }
        }

        private void CleanObjectives()
        {
            foreach (Transform child in objectivesListRoot)
            {
                Destroy(child.gameObject);
            }
        }
    }
}