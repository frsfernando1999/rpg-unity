﻿using UnityEngine;
using RPG.Dialogue;
using TMPro;
using UnityEngine.UI;

namespace RPG.UI
{
    public class DialogueUI : MonoBehaviour
    {
        private PlayerConversant _playerConversant;
        [SerializeField] private TextMeshProUGUI speaker;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Transform choiceRoot;
        [SerializeField] private GameObject responseButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button closeButton;

        private void Awake()
        {
            _playerConversant = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerConversant>();
        }

        private void Start()
        {
            ToggleDialogueBox();
        }

        private void OnEnable()
        {
            nextButton.onClick.AddListener(NextDialogue);
            closeButton.onClick.AddListener(CloseDialogue);
            _playerConversant.DialogueStarted += ToggleDialogueBox;
            _playerConversant.DialogueUpdated += UpdateUI;
        }

        private void OnDisable()
        {
            nextButton.onClick.RemoveListener(NextDialogue);
            closeButton.onClick.RemoveListener(CloseDialogue);
            _playerConversant.DialogueStarted -= ToggleDialogueBox;
            _playerConversant.DialogueUpdated -= UpdateUI;
        }

        private void ToggleDialogueBox()
        {
            ToggleBoxes();
            UpdateUI();
        }

        private void ToggleBoxes()
        {
            var box = GetComponent<Image>();
            if (box != null)
            {
                box.enabled = !box.enabled;
            }

            foreach (Transform child in GetComponentInChildren<Transform>())
            {
                child.gameObject.SetActive(!child.gameObject.activeSelf);
            }
        }
        
        private void CloseDialogue()
        {
            ToggleBoxes();
            foreach (Transform child in choiceRoot)
            {
                Destroy(child.gameObject);
            }
            _playerConversant.ResetDialogue();
        }

        private void NextDialogue()
        {
            if (_playerConversant.HasNextDialogue())
            {
                _playerConversant.NextDialogue();
            }
            else
            {
                _playerConversant.ResetDialogue();
            }
        }

        private void NextChoiceDialogue(DialogueNode choice)
        {
            _playerConversant.ButtonPressedTrigger(choice);
            if (_playerConversant.HasNextDialogueAfterChoice(choice))
            {
                _playerConversant.NextDialogueForGivenChoice(choice);
            }
            else
            {
                ToggleBoxes();
                _playerConversant.ResetDialogue();
            }
        }

        private void UpdateUI()
        {
            text.text = _playerConversant.GetText();
            speaker.text = _playerConversant.GetSpeaker();
            if (_playerConversant.IsChoiceDialogue())
            {
                CreateChoiceDialogue();
            }
            else
            {
                CreateNormalDialogue();
            }
        }
        
        private void CreateNormalDialogue()
        {
            choiceRoot.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(_playerConversant.HasNextDialogue());
        }

        private void CreateChoiceDialogue()
        {
            choiceRoot.gameObject.SetActive(true);
            nextButton.gameObject.SetActive(false);

            DestroyButtons();
            CreateButtons();
        }

        private void CreateButtons()
        {
            foreach (DialogueNode choice in _playerConversant.GetChoices())
            {
                GameObject buttonInstance = Instantiate(responseButton, choiceRoot);
                var buttonText = buttonInstance.GetComponentInChildren<TextMeshProUGUI>();
                if (buttonText != null)
                {
                    buttonText.text = choice.GetText();
                }

                var choiceButton = buttonInstance.GetComponent<Button>();
                choiceButton.onClick.AddListener(delegate { NextChoiceDialogue(choice); });
            }
        }

        private void DestroyButtons()
        {
            foreach (Transform item in choiceRoot)
            {
                Destroy(item.gameObject);
            }
        }
        
    }
}