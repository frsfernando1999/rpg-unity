using UnityEngine;

namespace RPG.UI.DamageText
{
    public class DestroyerScript : MonoBehaviour
    {
        [SerializeField] private GameObject target;
        public void DestroyText()
        {
            Destroy(target);
        }
    }
}