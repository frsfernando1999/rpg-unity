using UnityEngine;

namespace RPG.UI.DamageText
{
    public class DamageTextSpawner : MonoBehaviour
    {
        [SerializeField] private DamageText damageTextToSpawn;
        
        public void Spawn(float damageAmount)
        {
            var damageText = Instantiate(damageTextToSpawn, transform);
            
                damageText.SetText(damageAmount.ToString());
            
        }
    }
}