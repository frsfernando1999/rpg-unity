using UnityEngine;
using UnityEngine.UI;

namespace RPG.UI.DamageText
{
    public class DamageText : MonoBehaviour
    {
        [SerializeField] private Text damageText;
        public void SetText(string damage)
        {
            if (damageText == null) return;
            damageText.text = damage;
        }
    }
}
