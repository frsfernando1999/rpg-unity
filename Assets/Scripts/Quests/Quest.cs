﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using UnityEngine;
using UnityEngine.Serialization;

namespace RPG.Quests
{
    [CreateAssetMenu(fileName = "New Quest", menuName = "Quests/New Quest", order = 0)]
    public class Quest : ScriptableObject
    {
        [SerializeField] private List<Objective> objectives = new List<Objective>();
        [SerializeField] private List<Reward> rewards = new List<Reward>();

        [Serializable]
        public class Reward
        {
            [Min(1)] public int amount = 1;
            public InventoryItem item;
        }
        [Serializable]
        public class Objective
        {
            public string reference;
            public string description;
        }

        public IEnumerable<Objective> Objectives => objectives;
        public string Title => name;
        public int Progress => objectives.Count;

        public bool HasObjective(string reference)
        {
            foreach (var objective in objectives)
            {
                if (objective.reference == reference)
                {
                    return true;
                }
            }
            return false;
        }

        public IEnumerable<Reward> GetRewards()
        {
            return rewards;
        }

        public static Quest GetByName(string questName)
        {
            foreach (Quest quest in Resources.LoadAll<Quest>(""))
            {
                if (quest.name == questName)
                {
                    return quest;
                }
            }
            return null;
        }

    }
}