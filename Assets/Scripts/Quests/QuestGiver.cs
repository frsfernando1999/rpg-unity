using UnityEngine;

namespace RPG.Quests
{
    public class QuestGiver : MonoBehaviour
    {
        [SerializeField] private Quest quest;

        public void GiveQuest()
        {
            QuestList questList = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestList>();
            if (questList.AlreadyHasQuest(quest))
            {
                print("Already has quest");
                return;
            }
            questList.AddQuest(quest);
        }
    }
}