using System;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using Newtonsoft.Json.Linq;
using RPG.Core;
using RPG.Saving.Json;
using UnityEngine;

namespace RPG.Quests
{
    public class QuestList : MonoBehaviour, IJsonSaveable, IPredicateEvaluator
    {
        private List<QuestStatus> _statuses = new List<QuestStatus>();
        public event Action UpdatedQuestList;

        public IEnumerable<QuestStatus> GetStatuses()
        {
            return _statuses;
        }
        
        public void AddQuest(Quest quest)
        {
            QuestStatus newStatus = new QuestStatus(quest);
            _statuses.Add(newStatus);
            UpdatedQuestList?.Invoke();
        }

        public bool AlreadyHasQuest(Quest quest)
        {
            QuestStatus specificQuest = GetSpecificQuestStatus(quest);
            return specificQuest != null;
        }

        public QuestStatus GetSpecificQuestStatus(Quest quest)
        {
            var status = _statuses.FirstOrDefault(status => status.GetQuest() == quest);
            return status;
        }

        public void  CompleteObjective(Quest quest, string objective)
        {
            QuestStatus questStatus = GetSpecificQuestStatus(quest);
            if (questStatus == null)
            {
                Debug.Log("You don't have this quest yet");
                return;
            }
            if (!questStatus.IsObjectiveComplete(objective))
            {
                questStatus.AddCompletedObjective(objective);
                if (questStatus.IsQuestComplete())
                {
                    GiveReward(quest);
                }
                UpdatedQuestList?.Invoke();
            }
            else
            {
                Debug.Log("Already completed objective");
            }
        }

        private void GiveReward(Quest quest)
        {
            foreach (var reward in quest.GetRewards())
            {
                Inventory inventory = GetComponent<Inventory>();
                if (reward.item.IsStackable())
                {
                    bool successful = inventory.AddToFirstEmptySlot(reward.item, reward.amount);
                    if (!successful)
                    {
                        GetComponent<ItemDropper>().DropItem(reward.item, reward.amount);
                    }
                }
                else
                {
                    int i = 0;
                    while (i < reward.amount)
                    {
                        bool successful = inventory.AddToFirstEmptySlot(reward.item, 1);
                        if (!successful)
                        {
                            GetComponent<ItemDropper>().DropItem(reward.item, 1);
                        }
                        i++;
                    }
                }
                
            }
        }

        public JToken CaptureAsJToken()
        {
            JArray state = new JArray();
            IList<JToken> stateList = state;
            foreach (QuestStatus status in _statuses)
            {
                stateList.Add(status.CaptureAsJToken());
            }
            return state;

        }

        public void RestoreFromJToken(JToken state)
        {
            if (state is JArray stateArray)
            {
                _statuses.Clear();
                IList<JToken> stateList = stateArray;
                foreach (JToken token in stateList)
                {
                    _statuses.Add(new QuestStatus(token));
                }
            }
        }

        public bool? Evaluate(QuestPredicate predicate, string[] parameters)
        {
            if (predicate != QuestPredicate.HasQuest) return null;
            return AlreadyHasQuest(Quest.GetByName(parameters[0]));
        }
    }
}