using System.Linq;
using RPG.Quests;
using UnityEngine;

public class QuestCompletion : MonoBehaviour
{
    [SerializeField] private Quest quest;
    [SerializeField] private string reference;
    
    public void CompleteObjective()
    {
        QuestList questList = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestList>();
        if (quest.HasObjective(reference))
        {
            questList.CompleteObjective(quest, reference);
        }
        else
        {
            Debug.Log("Objectives don't match");
        }
    }
}
