using RPG.Control;
using UnityEngine;

namespace RPG.Dialogue
{
    public class AIConversant : MonoBehaviour, IRaycastable
    {
        [SerializeField] private Dialogue dialogue;
        [SerializeField] private string npcName;
        private bool _dead;

        public void SetDead()
        {
            _dead = true;
        }

        public string GetNpcName() => npcName;

        public CursorType GetCursorType()
        {
            return CursorType.Dialog;
        }

        public bool HandleRaycast(PlayerController callingController)
        {
            if (dialogue == null || _dead) return false;
            
            if (Input.GetMouseButtonDown(0))
            {
                var playerConversant = callingController.gameObject.GetComponent<PlayerConversant>();
                playerConversant.StartDialogue(dialogue, this);
                callingController.enabled = false;
            }

            return true;
        }
    }
}