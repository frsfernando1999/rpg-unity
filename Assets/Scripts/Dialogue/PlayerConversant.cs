using System;
using System.Collections.Generic;
using System.Linq;
using RPG.Control;
using RPG.Core;
using UnityEngine;

namespace RPG.Dialogue
{
    public class PlayerConversant : MonoBehaviour
    {
        [SerializeField] private string playerName;
        private AIConversant _currentConversant;
        private Dialogue _currentDialogue;
        private DialogueNode _currentNode;
        private string _npcName;

        public event Action DialogueStarted;
        public event Action DialogueUpdated;

        private PlayerInputs _controls;

        public void StartDialogue(Dialogue newDialogue, AIConversant npc)
        {
            _currentDialogue = newDialogue;
            _currentNode = _currentDialogue.GetRootNode();
            _currentConversant = npc;
            _npcName = npc.GetNpcName();
            TriggerEnterAction();
            DialogueStarted?.Invoke();
        }

        public string GetText()
        {
            if (_currentNode == null) return "";
            return _currentNode.GetText();
        }

        public string GetSpeaker()
        {
            if (_currentNode == null) return "";
            return _currentNode.GetSpeaker();
        }

        public string GetNpcName()
        {
            return _npcName;
        }

        public bool IsChoiceDialogue()
        {
            if (_currentNode == null) return false;
            return _currentNode.GetChildren().Count > 1;
        }


        private IEnumerable<DialogueNode> FilterOnCondition(IEnumerable<DialogueNode> inputNodes)
        {
            foreach (DialogueNode node in inputNodes)
            {
                if (node.CheckCondition(GetEvaluators()))
                {
                    yield return node;
                }
            }
        }

        private IEnumerable<IPredicateEvaluator> GetEvaluators()
        {
            return GetComponents<IPredicateEvaluator>();
        }

        public IEnumerable<DialogueNode> GetChoices()
        {
            List<DialogueNode> choiceNodes = new List<DialogueNode>();
            foreach (string child in _currentNode.GetChildren())
            {
                choiceNodes.Add(_currentDialogue.GetNodeByID(child));
            }
            return FilterOnCondition(choiceNodes);
        }

        public void NextDialogue()
        {
            DialogueNode[] children = _currentDialogue.GetAllChildren(_currentNode).ToArray();

            TriggerExitAction();
            _currentNode = children[0];
            TriggerEnterAction();

            DialogueUpdated?.Invoke();
        }

        public void NextDialogueForGivenChoice(DialogueNode choseNode)
        {
            DialogueNode nextNode = _currentDialogue.GetNodeByID(choseNode.GetSingleChild());

            TriggerExitAction();
            _currentNode = nextNode;
            TriggerEnterAction();

            DialogueUpdated?.Invoke();
        }

        public bool HasNextDialogue()
        {
            if (_currentNode == null) return false;
            return _currentNode.GetChildren().Count != 0;
        }

        public bool HasNextDialogueAfterChoice(DialogueNode choice)
        {
            if (choice == null) return false;
            return choice.GetChildren().Count != 0;
        }

        public void ResetDialogue()
        {
            TriggerExitAction();
            _currentDialogue = null;
            _currentNode = null;
            _currentConversant = null;
            _npcName = null;
            GetComponent<PlayerController>().enabled = true;
        }

        private void TriggerEnterAction()
        {
            if (_currentNode != null) TriggerFunction(_currentNode.GetEnterAction());
        }

        private void TriggerExitAction()
        {
            if (_currentNode != null) TriggerFunction(_currentNode.GetExitAction());
        }

        public void ButtonPressedTrigger(DialogueNode choice)
        {
            if (choice != null) TriggerFunction(choice.GetTriggerAction());
        }

        private void TriggerFunction(DialogueAction action)
        {
            DialogueTrigger[] triggers = _currentConversant.gameObject.GetComponents<DialogueTrigger>();
            foreach (var trigger in triggers)
            {
                if (trigger != null && action != DialogueAction.None)
                {
                    trigger.Trigger(action);
                }
            }
        }
    }
}