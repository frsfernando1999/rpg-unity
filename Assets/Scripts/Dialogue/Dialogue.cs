using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace RPG.Dialogue
{
    [CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue", order = 0)]
    public class Dialogue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private List<DialogueNode> nodes = new List<DialogueNode>();
        private Dictionary<string, DialogueNode> _nodeDictionary = new Dictionary<string, DialogueNode>();

#if UNITY_EDITOR

        private void OnValidate()
        {
            if (nodes.Count <= 0) return;

            _nodeDictionary.Clear();

            foreach (DialogueNode node in GetAllNodes())
            {
                if (node == null) return;
                _nodeDictionary[node.name] = node;
            }
        }
#endif

        public IEnumerable<DialogueNode> GetAllNodes()
        {
            return nodes;
        }

        public DialogueNode GetRootNode()
        {
            if (nodes.Count == 0) return null;
            return nodes[0];
        }

        public IEnumerable<DialogueNode> GetAllChildren(DialogueNode parentNode)
        {
            foreach (string childID in parentNode.GetChildren())
            {
                if (_nodeDictionary.ContainsKey(childID))
                {
                    yield return _nodeDictionary[childID];
                }
            }
        }

        public DialogueNode GetNodeByID(string id)
        {
            var foundNode = nodes.FirstOrDefault(node => node.name == id);
            return foundNode;
        }

#if UNITY_EDITOR
        public void CreateNode(DialogueNode parent)
        {
            var newNode = MakeNode(parent);
            Undo.RegisterCreatedObjectUndo(newNode, "Created a new Dialogue Node");
            Undo.RecordObject(this, "Added Dialogue Node");
            AddNode(newNode);
        }

        private DialogueNode MakeNode(DialogueNode parent)
        {
            DialogueNode newNode = CreateInstance<DialogueNode>();
            newNode.name = Guid.NewGuid().ToString();
            if (parent != null)
            {
                parent.AddChild(newNode.name);
                newNode.SetRectSize(parent.GetRect().size);
                var newNodePosition = GetNodePosition(parent.GetRect());
                newNode.SetRectPosition(newNodePosition);
            }
            return newNode;
        }

        private void AddNode(DialogueNode newNode)
        {
            nodes.Add(newNode);
            OnValidate();
        }

        private Vector2 GetNodePosition(Rect nodeRect)
        {
            return new Vector2(nodeRect.position.x + nodeRect.width + 20f, nodeRect.position.y);
        }

        public void DeleteNode(DialogueNode nodeToDelete)
        {
            if (nodes.Count <= 1) return;
            Undo.RecordObject(this, "Removed Dialogue Node");
            nodes.Remove(nodeToDelete);
            OnValidate();
            CleanDanglingChildren(nodeToDelete);
            Undo.DestroyObjectImmediate(nodeToDelete);
        }

        private void CleanDanglingChildren(DialogueNode nodeToDelete)
        {
            foreach (DialogueNode node in GetAllNodes())
            {
                node.RemoveChild(nodeToDelete.name);
            }
        }

#endif
        public void OnBeforeSerialize()
        {
#if UNITY_EDITOR
            if (AssetDatabase.GetAssetPath(this) != "")
            {
                if (nodes.Count == 0)
                {
                    var newNode = MakeNode(null);
                    AddNode(newNode);
                }

                foreach (var node in GetAllNodes())
                {
                    if (AssetDatabase.GetAssetPath(node) == "")
                    {
                        AssetDatabase.AddObjectToAsset(node, this);
                    }
                }
            }
#endif
        }

        public void OnAfterDeserialize()
        {
        }
    }
}