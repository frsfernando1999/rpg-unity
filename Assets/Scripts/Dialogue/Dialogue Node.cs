using System;
using System.Collections.Generic;
using RPG.Core;
using UnityEditor;
using UnityEngine;

namespace RPG.Dialogue
{
    [Serializable]
    public class DialogueNode : ScriptableObject
    {
        [SerializeField] private string speaker;
        [SerializeField] private string text;

        [SerializeField]
#if UNITY_EDITOR
        [ShowOnly]
#endif
        private List<string> children = new List<string>();

        [HideInInspector] [SerializeField] private Rect rect = new(2750, 2650, 300, 240);
        [HideInInspector] public bool hasAction = true;

        [SerializeField] private DialogueAction enterAction;
        [SerializeField] private DialogueAction exitAction;
        [SerializeField] private DialogueAction triggerAction;

        [SerializeField] private Condition condition;

        public DialogueAction GetEnterAction()
        {
            return enterAction;
        }

#if UNITY_EDITOR
        public void SetEnterAction(DialogueAction action)
        {
            Undo.RecordObject(this, "Changed Enter Action");
            enterAction = action;
            EditorUtility.SetDirty(this);
        }
#endif


        public DialogueAction GetExitAction()
        {
            return exitAction;
        }

#if UNITY_EDITOR
        public void SetExitAction(DialogueAction action)
        {
            Undo.RecordObject(this, "Changed Exit Action");
            exitAction = action;
            EditorUtility.SetDirty(this);
        }
#endif


        public string GetSpeaker()
        {
            return speaker;
        }

#if UNITY_EDITOR

        public void SetSpeaker(string newSpeaker)
        {
            if (speaker != newSpeaker)
            {
                Undo.RecordObject(this, "Changed Speaker");
                speaker = newSpeaker;
                EditorUtility.SetDirty(this);
            }
        }
#endif

        public string GetText()
        {
            return text;
        }
#if UNITY_EDITOR

        public void SetText(string newText)
        {
            if (text != newText)
            {
                Undo.RecordObject(this, "Changed Text");
                text = newText;
                EditorUtility.SetDirty(this);
            }
        }
#endif
        public List<string> GetChildren()
        {
            return children;
        }

        public string GetSingleChild()
        {
            if (children.Count == 0) return null;
            return children[0];
        }
#if UNITY_EDITOR
        public void AddChild(string child)
        {
            Undo.RecordObject(this, "Added a dialogue link");
            children.Add(child);
            EditorUtility.SetDirty(this);
        }

        public void RemoveChild(string child)
        {
            Undo.RecordObject(this, "Removed a dialogue link");
            children.Remove(child);
            EditorUtility.SetDirty(this);
        }
#endif

        public Rect GetRect()
        {
            return rect;
        }
#if UNITY_EDITOR

        public void SetRectPosition(Vector2 newPosition)
        {
            Undo.RecordObject(this, "Changed Rect Position");
            rect.position = newPosition;
            EditorUtility.SetDirty(this);
        }

        public void SetRectSize(Vector2 newSize)
        {
            Undo.RecordObject(this, "Changed Rect Size");
            rect.size = newSize;
            EditorUtility.SetDirty(this);
        }
#endif
        public DialogueAction GetTriggerAction()
        {
            return triggerAction;
        }

#if UNITY_EDITOR
        public void SetTriggerAction(DialogueAction action)
        {
            Undo.RecordObject(this, "Changed Trigger Action");
            triggerAction = action;
            EditorUtility.SetDirty(this);
        }
#endif
        public bool CheckCondition(IEnumerable<IPredicateEvaluator> evaluators)
        {
           return condition.Check(evaluators);
        }
    }

#if UNITY_EDITOR
    public class ShowOnlyAttribute : PropertyAttribute
    {
    }
#endif
}