using System;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;


namespace RPG.Dialogue.Editor
{
    public class DialogueEditor : EditorWindow
    {
        private Dialogue _selectedDialogue;

        [NonSerialized] private GUIStyle _nodeStyle;

        [NonSerialized] private DialogueNode _draggingNode;

        [NonSerialized] private DialogueNode _creatingNode;

        [NonSerialized] private DialogueNode _deletingNode;

        [NonSerialized] private DialogueNode _linkingParent;

        [NonSerialized] private bool _draggingCanvas;
        [NonSerialized] private Vector2 _draggingCanvasOffset;

        private Vector2 _scrollPosition;
        private Vector2 _gridOffset;
        private Vector2 _gridDrag;

        private Vector2 _draggingOffset;

        private const float CanvasSize = 5000f;
        private const float BackgroundSize = 50f;

        [MenuItem("Custom Windows/Dialogue Editor")]
        public static void ShowEditorWindow()
        {
            GetWindow(typeof(DialogueEditor), false, "Dialogue Editor");
        }

        private void OnEnable()
        {
            _scrollPosition = new Vector2(CanvasSize / 2, CanvasSize / 2);
            Selection.selectionChanged += OnSelectionChanged;
            _nodeStyle = new GUIStyle();
            _nodeStyle.normal.background = EditorGUIUtility.Load("node0") as Texture2D;
            _nodeStyle.padding = new RectOffset(20, 20, 20, 20);
            _nodeStyle.border = new RectOffset(12, 12, 12, 12);
        }

        private void OnDisable()
        {
            Selection.selectionChanged -= OnSelectionChanged;
        }

        private void OnSelectionChanged()
        {
            Dialogue newDialogue = Selection.activeObject as Dialogue;
            if (newDialogue != null)
            {
                _selectedDialogue = newDialogue;
                Repaint();
            }
        }

        [OnOpenAsset(1)]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            if (EditorUtility.InstanceIDToObject(instanceID) is not Dialogue) return false;
            ShowEditorWindow();
            return true;
        }

        private void OnGUI()
        {
            if (_selectedDialogue == null)
            {
                EditorGUILayout.LabelField("No Dialogue Selected");
            }
            else
            {
                ProcessEvents();

                _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

                Rect canvas = GUILayoutUtility.GetRect(CanvasSize, CanvasSize);
                var backgroundImage = Resources.Load("background") as Texture2D;

                var texCoordinates = new Rect(0, 0, CanvasSize / BackgroundSize, CanvasSize / BackgroundSize);
                GUI.DrawTextureWithTexCoords(canvas, backgroundImage, texCoordinates);


                foreach (DialogueNode node in _selectedDialogue.GetAllNodes())
                {
                    DrawNode(node);
                }

                foreach (DialogueNode node in _selectedDialogue.GetAllNodes())
                {
                    DrawConnections(node);
                    if (Event.current.type == EventType.Repaint)
                    {
                        DrawActions(node);
                    }
                }

                EditorGUILayout.EndScrollView();

                if (_creatingNode != null)
                {
                    _selectedDialogue.CreateNode(_creatingNode);
                    _creatingNode = null;
                }

                if (_deletingNode != null)
                {
                    _selectedDialogue.DeleteNode(_deletingNode);
                    _deletingNode = null;
                }
            }
        }

        private void DrawActions(DialogueNode node)
        {
            var children = node.GetChildren();
            if (children.Count > 1)
            {
                foreach (string child in children)
                {
                    _selectedDialogue.GetNodeByID(child).hasAction = false;
                }
            }
            else if (children.Count == 1)
            {
                _selectedDialogue.GetNodeByID(children[0]).hasAction = true;
            }

            GUI.changed = true;
        }

        private void DrawNode(DialogueNode node)
        {
            GUILayout.BeginArea(node.GetRect(), _nodeStyle);
            EditorStyles.textArea.wordWrap = true;

            EditorGUILayout.LabelField(node.GetSpeaker() + "'s Dialogue");
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Speaker: ", GUILayout.Width(node.GetRect().width * 0.2f));
            node.SetSpeaker(EditorGUILayout.TextField(node.GetSpeaker()));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Dialogue Text: ");
            node.SetText(EditorGUILayout.TextField(node.GetText(), EditorStyles.textArea,
                GUILayout.ExpandHeight(true)));
            EditorGUILayout.Separator();
            if (node.hasAction)
            {
                node.SetEnterAction((DialogueAction)EditorGUILayout.EnumPopup("Enter Action", node.GetEnterAction()));
                EditorGUILayout.Separator();
                node.SetExitAction((DialogueAction)EditorGUILayout.EnumPopup("Exit Action", node.GetExitAction()));
                EditorGUILayout.Separator();
            }
            else
            {
                node.SetTriggerAction((DialogueAction)EditorGUILayout.EnumPopup("Trigger Action", node.GetTriggerAction()));
                EditorGUILayout.Separator();
            }

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("x"))
            {
                _deletingNode = node;
            }

            if (_linkingParent == null && _linkingParent != node)
            {
                if (GUILayout.Button("Link"))
                {
                    _linkingParent = node;
                }
            }
            else if (_linkingParent != null && _linkingParent == node)
            {
                if (GUILayout.Button("Cancel"))
                {
                    _linkingParent = null;
                }
            }
            else if (_linkingParent != null && _linkingParent.GetChildren().Contains(node.name))
            {
                if (GUILayout.Button("Unlink"))
                {
                    _linkingParent.RemoveChild(node.name);
                    _linkingParent = null;
                }
            }
            else
            {
                if (GUILayout.Button("Child"))
                {
                    _linkingParent.AddChild(node.name);
                    _linkingParent = null;
                }
            }

            if (GUILayout.Button("+"))
            {
                _creatingNode = node;
            }

            GUILayout.EndHorizontal();

            GUILayout.EndArea();
        }

        private void DrawConnections(DialogueNode node)
        {
            Vector3 startPosition = new Vector2(node.GetRect().xMax, node.GetRect().center.y);
            foreach (DialogueNode child in _selectedDialogue.GetAllChildren(node))
            {
                var childNodeLeft = new Vector2(child.GetRect().xMin, child.GetRect().center.y);

                Vector3 endPosition = childNodeLeft;

                Vector3 controlPointOffset = endPosition - startPosition;
                controlPointOffset.y = 0;
                controlPointOffset.x *= 0.6f;

                Handles.DrawBezier(startPosition, endPosition, startPosition + controlPointOffset,
                    endPosition - controlPointOffset, Color.white, null, 4f);
            }
        }

        private void ProcessEvents()
        {
            if (Event.current.type == EventType.MouseDown && _draggingNode == null)
            {
                _draggingNode = GetNodeAtPoint(Event.current.mousePosition + _scrollPosition);
                if (_draggingNode != null)
                {
                    _draggingOffset = _draggingNode.GetRect().position - Event.current.mousePosition;
                    Selection.activeObject = _draggingNode;
                }
                else
                {
                    _draggingCanvas = true;
                    _draggingCanvasOffset = Event.current.mousePosition + _scrollPosition;
                    Selection.activeObject = _selectedDialogue;
                }
            }
            else if (Event.current.type == EventType.MouseDrag && _draggingNode != null)
            {
                _draggingNode.SetRectPosition(Event.current.mousePosition + _draggingOffset);
                GUI.changed = true;
            }
            else if (Event.current.type == EventType.MouseDrag && _draggingCanvas)
            {
                _scrollPosition = _draggingCanvasOffset - Event.current.mousePosition;
                GUI.changed = true;
            }
            else if (Event.current.type == EventType.MouseUp && _draggingNode != null)
            {
                _draggingNode = null;
            }
            else if (Event.current.type == EventType.MouseUp && _draggingCanvas)
            {
                _draggingCanvas = false;
            }

            if (Event.current.type == EventType.ValidateCommand && Event.current.commandName == "UndoRedoPerformed")
            {
                GUI.changed = true;
            }
        }

        private DialogueNode GetNodeAtPoint(Vector2 point)
        {
            DialogueNode foundNode = null;
            foreach (DialogueNode node in _selectedDialogue.GetAllNodes())
            {
                if (node.GetRect().Contains(point))
                {
                    foundNode = node;
                }
            }

            return foundNode;
        }
    }
}