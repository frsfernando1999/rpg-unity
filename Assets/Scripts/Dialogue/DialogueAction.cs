﻿namespace RPG.Dialogue
{
    public enum DialogueAction
    {
        None,
        Attack,
        GiveQuest,
        CompleteObjective
    }
}