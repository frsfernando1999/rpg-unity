﻿namespace RPG.Core
{
    public enum QuestPredicate
    {
        Select,
        HasQuest,
        CompletedObjective,
        CompletedQuest,
        HasLevel,
        MinimumTrait,
        HasItem,
        HasItems,
        HasItemEquipped
    }
}