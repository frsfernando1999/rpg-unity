namespace RPG.Core
{
    public interface IPredicateEvaluator
    {
        bool? Evaluate(QuestPredicate predicate, string[] parameters);
    }
}