using UnityEngine;

namespace RPG.Utils.Audio
{
    public class SoundRandomizer : MonoBehaviour
    {
        [SerializeField] private AudioClip[] audioClipArray;
        [SerializeField] private AudioSource referenceSource;
        [SerializeField] private float minPitch = 1f;
        [SerializeField] private float maxPitch = 1.5f;


        private int _audioClipIndex;
        private int[] _previousArray;
        private int _previousArrayIndex;

        private AudioClip GetRandomAudioClip()
        {
            if (_previousArray == null)
            {
                _previousArray = new int[audioClipArray.Length / 2];
            }

            if (_previousArray.Length == 0)
            {
                return null;
            }

            do
            {
                _audioClipIndex = Random.Range(0, audioClipArray.Length);
            } while (PreviousArrayContainsAudioClipIndex());

            _previousArray[_previousArrayIndex] = _audioClipIndex;
            _previousArrayIndex++;
            if (_previousArrayIndex >= _previousArray.Length)
            {
                _previousArrayIndex = 0;
            }

            return audioClipArray[_audioClipIndex];
        }

        private bool PreviousArrayContainsAudioClipIndex()
        {
            foreach (var t in _previousArray)
            {
                if (t == _audioClipIndex)
                {
                    return true;
                }
            }

            return false;
        }

        public void PlaySound()
        {
            if (referenceSource == null) return;
            referenceSource.clip = GetRandomAudioClip();
            referenceSource.pitch = Random.Range(minPitch, maxPitch);
            referenceSource.Play();
        }
    }
}