using System.Collections;
using RPG.Control;
using RPG.Saving.Json;
using UnityEngine;
using UnityEngine.InputSystem;

namespace RPG.SceneManagement
{
    public class SavingWrapper : MonoBehaviour, PlayerInputs.ISavingActions
    {
        private const string DefaultSave = "save";
        private bool _isTransitioning;
        [SerializeField] private float loadTime = 2f;
        [SerializeField] private float fadeOutTime = 1f;

        private PlayerInputs _controls;

        public void OnEnable()
        {
            if (_controls == null)
            {
                _controls = new PlayerInputs();
                _controls.Saving.SetCallbacks(this);
            }
            _controls.Saving.Enable();
        }
        
        public void OnDisable()
        {
            _controls?.Saving.Disable();
        }


        public void SetIsTransitioning(bool isTransitioning) => _isTransitioning = isTransitioning;

        private void Awake()
        {
            StartCoroutine(LoadLastScene());
        }

        private IEnumerator LoadLastScene()
        {
            yield return GetComponent<JsonSavingSystem>().LoadLastScene(DefaultSave);
            Fader fader = FindObjectOfType<Fader>();
            GameObject.FindWithTag("Player").GetComponent<PlayerController>().enabled = false;
            fader.InstantFadeIn();
            yield return new WaitForSeconds(fadeOutTime);
            yield return fader.FadeOut(loadTime);
            GameObject.FindWithTag("Player").GetComponent<PlayerController>().enabled = true;
        }
        
        private void Delete()
        {
            GetComponent<JsonSavingSystem>().Delete(DefaultSave);
        }

        public void Load()
        {
            GetComponent<JsonSavingSystem>().Load(DefaultSave);
        }

        public void Save()
        {
            GetComponent<JsonSavingSystem>().Save(DefaultSave);
        }

        public void OnSave(InputAction.CallbackContext context)
        {
            if (context.performed && !_isTransitioning) Save();
        }

        public void OnDelete(InputAction.CallbackContext context)
        {
            if (context.performed && !_isTransitioning) Delete();
        }

        public void OnLoad(InputAction.CallbackContext context)
        {
            if (context.performed && !_isTransitioning) Load();
        }
    }
}