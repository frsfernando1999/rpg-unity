using System;
using System.Collections;
using RPG.Control;
using RPG.Core;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace RPG.SceneManagement
{
    enum DestinationIdentifier
    {
        A, B, C, D, E
    }

    public class Portal : MonoBehaviour
    {
        [SerializeField] private int sceneToLoad = -1;
        [SerializeField] private float loadFadeInTime = 1f;
        [SerializeField] private float loadFadeOutTime = 1f;
        [SerializeField] private float loadWaitTime = 0.5f;
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private DestinationIdentifier destination;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                StartCoroutine(Transition());
            }
        }
        

        private IEnumerator Transition()
        {
            if (sceneToLoad < 0) throw new Exception("Scene to travel not set!");

            DontDestroyOnLoad(gameObject);

            Fader fader = FindObjectOfType<Fader>();
            SavingWrapper savingWrapper = FindObjectOfType<SavingWrapper>();
            DisablePlayerMovementDuringLoad();
            
            yield return StartCoroutine(fader.FadeIn(loadFadeInTime));
            
            savingWrapper.SetIsTransitioning(true);
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<PlayerController>().enabled = false;
            savingWrapper.Save();
            
            yield return SceneManager.LoadSceneAsync(sceneToLoad);
            
            player = GameObject.FindGameObjectWithTag("Player");
            savingWrapper.Load();
            player.GetComponent<PlayerController>().enabled = false;
            Portal otherPortal = GetOtherPortal();
            UpdatePlayer(otherPortal);
            savingWrapper.Save();
            
            yield return new WaitForSeconds(loadWaitTime);
            
            yield return StartCoroutine(fader.FadeOut(loadFadeOutTime));
            
            savingWrapper.SetIsTransitioning(false);
            player.GetComponent<PlayerController>().enabled = true;
            
            Destroy(gameObject);
        }

        private static void DisablePlayerMovementDuringLoad()
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void UpdatePlayer(Portal otherPortal)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<NavMeshAgent>().enabled = false;
            player.transform.position = otherPortal.spawnPoint.position;
            player.transform.rotation = otherPortal.spawnPoint.rotation;
            player.GetComponent<NavMeshAgent>().enabled = true;
        }

        private Portal GetOtherPortal()
        {
            foreach (Portal portal in FindObjectsOfType<Portal>())
            {
                if (portal != this && portal.destination == destination)
                {
                    return portal;
                }
            }
            return null;
        }
    }
}