using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RPG.Saving.Binary
{
    public class SavingSystem : MonoBehaviour
    {
        private const string LastSceneBuildIndex = "lastSceneBuildIndex";

        public IEnumerator LoadLastScene(string saveFileName)
        {
            var saveFilePath = GetPathFromSaveFile(saveFileName);
            var state = LoadFile(saveFilePath);
            if (state.ContainsKey(LastSceneBuildIndex))
            {
                int sceneIndex = (int)state[LastSceneBuildIndex];
                if (sceneIndex != SceneManager.GetActiveScene().buildIndex)
                {
                    yield return SceneManager.LoadSceneAsync(sceneIndex);
                }
            }
            RestoreState(state);
        }

        public void Save(string saveFileName)
        {
            var saveFilePath = GetPathFromSaveFile(saveFileName);

            var state = LoadFile(saveFilePath);
            CaptureState(state);
            SaveFile(saveFilePath, state);
        }

        private void SaveFile(string saveFilePath, object captureState)
        {
            Debug.Log("Saving to " + saveFilePath);
            using (FileStream stream = File.Open(saveFilePath, FileMode.Create))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, captureState);
            }
        }

        public void Load(string saveFileName)
        {
            var saveFilePath = GetPathFromSaveFile(saveFileName);
            RestoreState(LoadFile(saveFilePath));
        }

        private Dictionary<string, object> LoadFile(string saveFilePath)
        {
            Debug.Log("Loading from " + saveFilePath);

            if (!File.Exists(saveFilePath))
            {
                return new Dictionary<string, object>();
            }

            using (FileStream saveFileDoc = File.Open(saveFilePath, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return (Dictionary<string, object>)formatter.Deserialize(saveFileDoc);
            }
        }

        private void CaptureState(Dictionary<string, object> state)
        {
            foreach (SaveableEntity saveable in FindObjectsOfType<SaveableEntity>())
            {
                state[saveable.GetUniqueIdentifier()] = saveable.CaptureState();
            }

            state[LastSceneBuildIndex] = SceneManager.GetActiveScene().buildIndex;
        }

        private void RestoreState(Dictionary<string, object> state)
        {
            foreach (var saveable in FindObjectsOfType<SaveableEntity>())
            {
                var uniqueIdentifier = saveable.GetUniqueIdentifier();

                if (state.ContainsKey(uniqueIdentifier))
                {
                    saveable.RestoreState(state[uniqueIdentifier]);
                }
            }
        }

        private string GetPathFromSaveFile(string saveFile)
        {
            return Path.Combine(Application.persistentDataPath, saveFile + ".sav");
        }
    }
}