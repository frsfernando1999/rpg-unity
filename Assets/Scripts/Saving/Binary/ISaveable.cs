﻿namespace RPG.Saving.Binary
{
    public interface ISaveable
    {
        object CaptureState();
        void RestoreState(object state);
    }
}