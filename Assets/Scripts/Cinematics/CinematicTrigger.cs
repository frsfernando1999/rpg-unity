using UnityEngine;
using UnityEngine.Playables;

namespace RPG.Cinematics
{
    public class CinematicTrigger : MonoBehaviour
    {
        private bool _triggered;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player") && !_triggered)
            {
                _triggered = true;
                GetComponent<PlayableDirector>().Play();
            }
        }
    }
}